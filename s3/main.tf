provider "aws" {
  region  = "${var.aws_region}"
  profile = "dev"
}

terraform {
  # Intentionally empty. Will be filled by Terragrunt.
  backend "s3" {}
  required_version = ">= 0.12.0"
}

resource "aws_s3_bucket" "poc_bucket" {
  bucket = "infrastructure-tf-test-bucket"
  acl    = "private"

  tags = {
    Name        = "infrastructure-tf-test-bucket"
    Environment = "company-dev"
  }
}
