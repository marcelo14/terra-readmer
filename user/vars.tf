variable "aws_region" {
  description = "The AWS region to deploy to (e.g. us-east-1)"
  type        = string
  default     = "us-east-1"
}
variable "username" {
  description = "List of usernames"
  type        = "list"
  default     = ["person1.poc", "marcelo.poc", "person2.poc"]
}
variable "test" {
  description = "Just a test"
  type        = "string"
  default     = "rock"
}
variable "test2" {
  description = "Just a test"
  type        = "string"
  default     = "blues"
}
