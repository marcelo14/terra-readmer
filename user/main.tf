provider "aws" {
  region  = "${var.aws_region}"
  profile = "dev"
}

terraform {
  # Intentionally empty. Will be filled by Terragrunt.
  backend "s3" {}
  required_version = ">= 0.12.0"
}

resource "aws_iam_user" "poc_users" {
  count = "${length(var.username)}"
  name  = "${element(var.username, count.index)}"
}
