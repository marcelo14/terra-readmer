provider "aws" {
  region  = "${var.aws_region}"
  profile = "dev"
}

terraform {
  # Intentionally empty. Will be filled by Terragrunt.
  backend "s3" {}
  required_version = ">= 0.12.0"
}

resource "aws_instance" "ec2" {
  ami                     = "${var.ami_id}"
  disable_api_termination = false
  ebs_optimized           = false
  monitoring              = false
  credit_specification {
    cpu_credits = "standard"
  }
  availability_zone      = "${var.az}"
  iam_instance_profile   = "${var.iam_instance_profile}"
  instance_type          = "${var.instance_type}"
  key_name               = "${var.key_name}"
  count                  = "${var.instance_count}"
  subnet_id              = "${var.subnet_id}"
  security_groups        = ["${var.security_groups}"]
  vpc_security_group_ids = ["${var.vpc_security_group_ids}"]
  root_block_device {
    delete_on_termination = false
    iops                  = 100
    volume_size           = 8
    volume_type           = "gp2"
  }
  ebs_block_device {
    delete_on_termination = false
    device_name           = "/dev/sdj"
    encrypted             = false
    iops                  = 100
    volume_size           = 8
    volume_type           = "gp2"
    snapshot_id           = "snap-whatever"
  }
  tags = {
    Name          = "${var.name}"
    environment   = "${var.environment}"
    hostname      = "${var.hostname}"
    puppetclass   = "${var.dumb}"
    system        = "${var.system}"
    "patch group" = "${var.patch_group}"
  }
  volume_tags = {
    environment = "${var.environment}"
    system      = "${var.system}"
  }
}
