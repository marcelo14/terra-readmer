variable "aws_region" {
  description = "The AWS region to deploy to (e.g. us-east-1)"
  default     = "us-east-1"
}

variable "ami_id" {
  description = "source ami id"
  default     = "ami-whatever"
}
variable "instance_count" {}

variable "az" {
  description = "availablility zone for the instance"
  default     = "us-east-1e"
}

variable "iam_instance_profile" {
  description = "iam profile of the instance"
  default     = "dumb"
}

variable "instance_type" {
  description = "ec2 machine type"
  default     = "t2.micro"
}

variable "key_name" {
  description = "key pair name"
  default     = "company-keypair"
}

variable "subnet_id" {
  description = "subnet id"
  default     = "subnet-whatever"
}

variable "security_groups" {
  description = "security group"
  default     = "sg-03b5beccfafdc5d90"
}

variable "vpc_security_group_ids" {
  description = "vpc security groups"
  default     = "sg-blabla"
}

variable "name" {
  description = "instance name"
  default     = "terraform_test"
}
variable "environment" {
  description = "instance environment"
  default     = "staging"
}

variable "hostname" {
  description = "hostname of the instance"
  default     = "terraform_test"
}
variable "puppetclass" {
  description = "puppet class file name"
  default     = "terraform_test18"
}

variable "system" {
  description = "system that owns this service"
  default     = "dumbdumb"
}

variable "patch_group" {
  description = "patch group name"
  default     = "test patch group"
}
